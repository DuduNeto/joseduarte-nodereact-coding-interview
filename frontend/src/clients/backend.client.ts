import axios from "axios";
import { IUserProps } from "../dtos/user.dto";

export class BackendClient {
  private readonly baseUrl: string;

  constructor(baseUrl = "http://localhost:3001/v1") {
    this.baseUrl = baseUrl;
  }

  async getAllUsers(filters: Record<string, string> = {}): Promise<{ data: IUserProps[] }> {
    const query = Object.entries(filters).map(filter => {
      return `${filter[0]}=${filter[1]}`;
    })
    return (await axios.get(`${this.baseUrl}/people/all?${query.join('&')}`, {})).data;
  }
}
