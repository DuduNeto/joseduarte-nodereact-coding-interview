import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [filters, setFilters] = useState<Record<string, string>>({});

  useEffect(() => {
    const fetchData = async () => {
      const result = await backendClient.getAllUsers(filters);
      setUsers(result.data);
    };

    fetchData();
  }, [filters]);

  const handleSubmitSearch = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const target = event.target as typeof event.target & {
      title: { value: string };
      gender: { value: string };
      company: { value: string };
    };
    const title = target.title.value;
    const gender = target.gender.value; 
    const company = target.company.value; 
    
    setFilters({
      title,
      gender,
      company,
    })
}

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{
        alignItems: "center",
        height: "64px",
        width: "100%",
        display: "flex",
        margin: "0px 0px 0px 10px"
      }}>
        <form 
        style={{display: "flex"}}
        onSubmit={handleSubmitSearch}
        >
          <div style={{
            display: "flex",
            flexDirection: "column",
            margin: 5,
          }}>
            <label>Title</label>
            <input name="title"></input>
          </div>
          <div style={{
            display: "flex",
            flexDirection: "column",
            margin: 5,
          }}>
            <label>Gender</label>
            <input name="gender"></input>
          </div>
          <div style={{
            display: "flex",
            flexDirection: "column",
            margin: 5,
          }}>
            <label>Company</label>
            <input name="company"></input>
          </div>
          <div style={{
            display: "flex",
            margin: 5,
            justifyContent: "flex-end"
          }}>
            <button type="submit" >Search</button>
          </div>
        </form>
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map((user) => {
                return <UserCard key={user.id} {...user} />;
              })
              : null}
          </div>
        )}
      </div>
    </div>
  );
};
