export interface IPeopleAll{
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    gender: string;
    title: string;
    company: string;
    avatar: string;
}