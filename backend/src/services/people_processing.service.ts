import { IPeopleAll } from '../common/types';
import people_data from '../data/people_data.json';

interface IIndexable {
    [key: string]: unknown;
  }

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getAll(filterFields: Partial<IPeopleAll>) {
        const filters = Object.entries(filterFields).filter(filter => !!filter[1]);
        return people_data.filter((people: IIndexable) => {
            const result = filters.every(filter => {
               return people[filter[0]] === filter[1]
            })
            return result
        });
    }
}
